import { Application, Container, Sprite, Texture, type ICanvas } from 'pixi.js';
import kevin from '../../assets/kevin.png';
import borrachin from '../../assets/borrachin.png';
import gloria from '../../assets/gloria.png';
import hermenejildoMalojillo from "../../assets/hermenejildo-malojillo.png" 
import hermenejildoMalojilla from "../../assets/hermenejilda-malojilla.png" 
import mamalonaEncapucha from "../../assets/mamalona-encapucha.png" 
import mamalonaEnojada from "../../assets/mamalona-enojada.png" 

export const applyToGame = (app:Application<ICanvas>)=>{    
    const container = new Container();
    app.stage.addChild(container);
    
    // Create a new texture
    const textures = [
        Texture.from(kevin),
        Texture.from(borrachin),
        Texture.from(gloria),
        Texture.from(hermenejildoMalojillo),
        Texture.from(hermenejildoMalojilla),
        Texture.from(mamalonaEncapucha),
        Texture.from(mamalonaEnojada),
    ];

    // Create a 5x5 grid of bunnies
        const bunny = new Sprite(textures[0]);
        // const n = 0.5
        // bunny.scale = {x:n, y:n};
        // bunny.anchor.set(-0.1,-0.01);
        // bunny.x = 0
        // bunny.y = 0
        // container.addChild(bunny);
    
    // Move container to the center
    container.x = app.screen.width / 2;
    container.y = app.screen.height / 2;
    
    // Center bunny sprite in local container coordinates
    container.pivot.x = container.width / 2;
    container.pivot.y = container.height / 2;
    let c = 0;
    setInterval(()=>{
        c = c >= textures.length? 0 : c;
        const n = 0.5
        bunny.scale = {x:n, y:n};
        bunny.anchor.set(.3,.55);
        bunny.x = 0
        bunny.y = 0
        container.addChild(bunny);
        bunny.texture = textures[c++]
    }, 500)
    app.ticker.add((delta)=>{
        container.rotation -= 0.01 * delta
    })
}